package com.mq.starter.cilent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lumingfu
 */
@SpringBootApplication
public class StarterCilentApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarterCilentApplication.class, args);
    }

}
