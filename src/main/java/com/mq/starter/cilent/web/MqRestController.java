package com.mq.starter.cilent.web;

import com.fire.mq.rabbitmq.FireRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mr.Fire -lmf
 * @date 2021/8/15 16:52
 * @desc
 */
@RestController
public class MqRestController {

    @Autowired
    FireRabbitTemplate rabbitTemplate;

    @GetMapping("/send")
    public String sendMsg(){
        String msg = "这是一条来自"+rabbitTemplate.getName()+"的消息！";
        rabbitTemplate.convertAndSend("helloQueue",msg);
        return "success";
    }

}
