package com.mq.starter.cilent.queue;

import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Mr.Fire -lmf
 * @date 2021/8/15 17:51
 * @desc
 */
@Configuration
public class HelloQueue {

    @Bean
    public org.springframework.amqp.core.Queue queue() {
        return new org.springframework.amqp.core.Queue("helloQueue");
    }
}
