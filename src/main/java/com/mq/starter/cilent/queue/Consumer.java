package com.mq.starter.cilent.queue;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;

/**
 * @author Mr.Fire -lmf
 * @date 2021/8/15 17:15
 * @desc
 */

@Configuration
public class Consumer {

    @RabbitListener(queues = "helloQueue")
    @RabbitHandler
    public void receive(String msg) {
        System.out.println("Consumer收到消息：" + msg);
    }

}
